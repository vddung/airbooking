import React from 'react'
import BookingFormControls from '../src/components/booking-form-controls-component'
import { FLIGHT_CLASSES as FC } from '../src/data/FLIGHT-CLASSES'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'react-datepicker/dist/react-datepicker.css'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import { Button, Welcome } from '@storybook/react/demo'

function mockFunc () {
  return true
}

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />)

storiesOf('Button', module)
  .add('with text', () => <Button onClick={action('clicked')}>Hello Button</Button>)
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>
      <span role='img' aria-label='so cool'>
        😀 😎 👍 💯
      </span>
    </Button>
  ))

storiesOf('AirFormControl', module)
  .add('Input', () => (
    <BookingFormControls.OptionFlightClass
      flightClasses={FC}
      selected='First Class'
      handleChange={mockFunc}
    />
  ))
  .add('DateTimePicker', () => (
    <BookingFormControls.TextBoxDateTimePicker
      selected={new Date()}
      handleChange={mockFunc}
      description='Please select departure date.'
    />
  ))
