import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import './App.css'
import { Container, Row, Col } from 'react-bootstrap'
import LeftMenu from './components/left-menu-component'
import TopMenu from './components/top-menu-component'
import Index from './components/index-component'
import BookingWizard from './components/booking-wizard-component'
import FlightsSchedule from './components/flights-schedule-component'
import Promotions from './components/promotions-component'
import MyBook from './components/my-book-component'

function App () {
  return (
    <BrowserRouter>
      <Container>
        <Row>
          <Col>
            <TopMenu />
          </Col>
        </Row>
        <Row>
          <Col md={2}>
            <LeftMenu />
          </Col>
          <Col md={10}>
            <Route path='/' exact component={Index} />
            <Route path='/myBook' exact component={MyBook} />
            <Route path='/bookingWizard' component={BookingWizard} />
            <Route path='/schedule' exact component={FlightsSchedule} />
            <Route path='/promotions' exact component={Promotions} />
          </Col>
        </Row>
      </Container>
    </BrowserRouter>
  )
}

export default App
