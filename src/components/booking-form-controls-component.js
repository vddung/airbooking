import React from 'react'
import { Form } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { isMaxLength, isEmpty } from '../utilities/validator'

function TextBoxAirPort (props) {
  var desc = props.description

  if (isEmpty(props.value)) {
    desc = 'This cannot be empty'
  } else if (isMaxLength(props.value)) {
    desc = 'Exceeded max length'
  }

  return (
    <>
      <Form.Control
        name='departure-airport'
        type='input'
        placeholder='Departure Airport'
        value={props.value}
        onChange={props.handleChange.bind(this)}
      />
      <Form.Text className='text-muted'>{desc}</Form.Text>
    </>
  )
}

function TextBoxDateTimePicker (props) {
  return (
    <>
      <DatePicker
        name='departure-time'
        selected={props.selected}
        onChange={props.handleChange}
        className='form-control date-picker'
      />
      <Form.Text className='text-muted'>{props.description}</Form.Text>
    </>
  )
}

function OptionFlightClass (props) {
  var flightClassesMap = props.flightClasses.map(fclass => (
    <option key={fclass} value={fclass}>
      {fclass}
    </option>
  ))
  return (
    <>
      <Form.Control
        name='departure-class'
        as='select'
        value={props.selected}
        onChange={props.handleChange.bind(this)}
      >
        {flightClassesMap}
      </Form.Control>
    </>
  )
}

function OptionNumberTravellers (props) {
  var ageOptionsArray = []

  for (var i = 0; i <= props.maxAttendant; i++) {
    ageOptionsArray.push(i)
  }

  var ageOptionsMap = ageOptionsArray.map(opt => (
    <option key={opt} value={opt}>
      {opt}
    </option>
  ))

  return (
    <>
      <Form.Control
        as='select'
        value={props.selected}
        onChange={props.handleChange.bind(this, props.ageGroup)}
      >
        {ageOptionsMap}
      </Form.Control>
      <Form.Text className='text-muted'>{props.description}</Form.Text>
    </>
  )
}

function TextBoxPromotionCode (props) {
  return (
    <>
      <Form.Control
        type='input'
        placeholder='Enter Code'
        value={props.value}
        onChange={props.handleChange.bind(this)}
      />
    </>
  )
}

export default {
  TextBoxAirPort,
  TextBoxDateTimePicker,
  OptionFlightClass,
  OptionNumberTravellers,
  TextBoxPromotionCode
}
