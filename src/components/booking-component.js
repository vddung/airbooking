import React, { Component } from 'react'
import { Row, Col, Form, Button } from 'react-bootstrap'
import produce from 'immer'
import BookingFormControls from './booking-form-controls-component'
import { FLIGHT_CLASSES as FC } from '../data/FLIGHT-CLASSES'
import { connect } from 'react-redux'
import { updateCurrentBooking } from '../redux/actions/actions'
import uniqid from 'uniqid'
import { Link, withRouter } from 'react-router-dom'

import 'react-datepicker/dist/react-datepicker.css'
import '../assets/css/form.css'

const mapStateToProps = (state, ownProps) => {
  return {
    reduxState: state
  }
}

const mapDispatchToProps = dispatch => {
  return {
    updateCurrentBooking: booking => dispatch(updateCurrentBooking(booking))
  }
}

class Booking extends Component {
  constructor (props) {
    super(props)
    this.state = {
      book: {
        id: '',
        type: 0,
        departure: {
          airport: '',
          time: new Date(),
          class: 'Economic Class'
        },
        arrival: {
          airport: '',
          time: new Date(),
          class: 'Economic Class'
        },
        travellers: {
          adult: 0,
          children: 0,
          infant: 0
        },
        promotion_code: ''
      }
    }

    this.handleDepartureAirportChange = this.handleDepartureAirportChange.bind(
      this
    )
    this.handleArrivalAirportChange = this.handleArrivalAirportChange.bind(this)
    this.handleDepartureDateChange = this.handleDepartureDateChange.bind(this)
    this.handleReturnDateChange = this.handleReturnDateChange.bind(this)
    this.handleDepartureFlightClassChange = this.handleDepartureFlightClassChange.bind(
      this
    )
    this.handleReturnFlightClassChange = this.handleReturnFlightClassChange.bind(
      this
    )
    this.handleNumberTravellersChange = this.handleNumberTravellersChange.bind(
      this
    )
    this.handlePromotionCodeChange = this.handlePromotionCodeChange.bind(this)
    this.handleFindFlightsClick = this.handleFindFlightsClick.bind(this)
  }

  handleDepartureAirportChange (e) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        departure: {
          ...this.props.reduxState.currentBooking.departure,
          airport: e.target.value
        }
      })
    )
  }

  handleArrivalAirportChange (e) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        arrival: {
          ...this.props.reduxState.currentBooking.arrival,
          airport: e.target.value
        }
      })
    )
  }

  handleDepartureDateChange (date) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        departure: {
          ...this.props.reduxState.currentBooking.departure,
          time: date
        }
      })
    )
  }

  handleReturnDateChange (date) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        arrival: {
          ...this.props.reduxState.currentBooking.arrival,
          time: date
        }
      })
    )
  }

  handleDepartureFlightClassChange (e) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        departure: {
          ...this.props.reduxState.currentBooking.departure,
          class: e.target.value
        }
      })
    )
  }

  handleReturnFlightClassChange (e) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        arrival: {
          ...this.props.reduxState.currentBooking.arrival,
          class: e.target.value
        }
      })
    )
  }

  handleNumberTravellersChange (ageGroup, e) {
    switch (ageGroup) {
      case 0:
        this.props.updateCurrentBooking(
          Object.assign({}, this.props.reduxState.currentBooking, {
            travellers: {
              ...this.props.reduxState.currentBooking.travellers,
              adult: e.target.value
            }
          })
        )
        break
      case 1:
        this.props.updateCurrentBooking(
          Object.assign({}, this.props.reduxState.currentBooking, {
            travellers: {
              ...this.props.reduxState.currentBooking.travellers,
              children: e.target.value
            }
          })
        )
        break
      case 2:
        this.props.updateCurrentBooking(
          Object.assign({}, this.props.reduxState.currentBooking, {
            travellers: {
              ...this.props.reduxState.currentBooking.travellers,
              infant: e.target.value
            }
          })
        )
        break
      default:
        this.props.updateCurrentBooking(
          Object.assign({}, this.props.reduxState.currentBooking, {
            travellers: {
              ...this.props.reduxState.currentBooking.travellers,
              adult: e.target.value
            }
          })
        )
    }
  }

  handlePromotionCodeChange (e) {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, {
        promotion_code: e.target.value
      })
    )
  }

  handleFindFlightsClick () {
    this.props.updateCurrentBooking(
      Object.assign({}, this.props.reduxState.currentBooking, { id: uniqid() })
    )
    this.props.history.push(this.props.nextStep)
  }

  render () {
    return (
      <>
        <h4>Where and When</h4>
        <br />
        <Row>
          <Col md>
            <Form>
              <Form.Row>
                <Form.Group as={Col} controlId='frmDepartureAirport'>
                  <BookingFormControls.TextBoxAirPort
                    handleChange={this.handleDepartureAirportChange}
                    value={
                      this.props.reduxState.currentBooking.departure.airport
                    }
                    description='Please select departure airport.'
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmDepartureDate'>
                  <BookingFormControls.TextBoxDateTimePicker
                    selected={
                      this.props.reduxState.currentBooking.departure.time
                    }
                    handleChange={this.handleDepartureDateChange}
                    description='Please select departure date.'
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmDepartureClass'>
                  <BookingFormControls.OptionFlightClass
                    flightClasses={FC}
                    selected={
                      this.props.reduxState.currentBooking.departure.class
                    }
                    handleChange={this.handleDepartureFlightClassChange}
                  />
                </Form.Group>
              </Form.Row>
              <Form.Row>
                <Form.Group as={Col} controlId='frmArrivalAirport'>
                  <BookingFormControls.TextBoxAirPort
                    handleChange={this.handleArrivalAirportChange}
                    value={this.props.reduxState.currentBooking.arrival.airport}
                    description='Please select arrival airport.'
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmReturnDate'>
                  <BookingFormControls.TextBoxDateTimePicker
                    selected={this.props.reduxState.currentBooking.arrival.time}
                    handleChange={this.handleReturnDateChange}
                    description='Please select return date.'
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmReturnClass'>
                  <BookingFormControls.OptionFlightClass
                    flightClasses={FC}
                    selected={
                      this.props.reduxState.currentBooking.arrival.class
                    }
                    handleChange={this.handleReturnFlightClassChange}
                  />
                </Form.Group>
              </Form.Row>
              <hr />
              <h4>Number of Travellers</h4>
              <br />
              <Form.Row>
                <Form.Group as={Col} controlId='frmAdults' md={2}>
                  <BookingFormControls.OptionNumberTravellers
                    maxAttendant={9}
                    ageGroup={0}
                    selected={
                      this.props.reduxState.currentBooking.travellers.adult
                    }
                    handleChange={this.handleNumberTravellersChange}
                    description={'{Age 12+}'}
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmChildren' md={2}>
                  <BookingFormControls.OptionNumberTravellers
                    maxAttendant={9}
                    ageGroup={1}
                    selected={
                      this.props.reduxState.currentBooking.travellers.children
                    }
                    handleChange={this.handleNumberTravellersChange}
                    description='(Age 2-11))'
                  />
                </Form.Group>
                <Form.Group as={Col} controlId='frmInfant' md={2}>
                  <BookingFormControls.OptionNumberTravellers
                    maxAttendant={9}
                    ageGroup={2}
                    selected={
                      this.props.reduxState.currentBooking.travellers.infant
                    }
                    handleChange={this.handleNumberTravellersChange}
                    description='(Age 0-1, no seat needed)'
                  />
                </Form.Group>
              </Form.Row>
              <hr />
              <h4>Promotional Offers</h4>
              <br />
              <Form.Row>
                <Form.Group as={Col} controlId='frmPromotionCode' md={4}>
                  <BookingFormControls.TextBoxPromotionCode
                    value={this.props.reduxState.currentBooking.promotion_code}
                    handleChange={this.handlePromotionCodeChange}
                  />
                </Form.Group>
              </Form.Row>
              <hr />
              <Button
                type='button'
                variant='danger'
                className='float-right'
                onClick={this.handleFindFlightsClick}
              >
                <b>Find Flights ></b>
              </Button>
            </Form>
          </Col>
        </Row>
      </>
    )
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Booking)
)
