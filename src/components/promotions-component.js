import React, { Component } from 'react'
import { Media } from 'react-bootstrap'

import NhaTrangPic from '../assets/img/NT.jpg'
import TokyoPic from '../assets/img/FujiMT.jpg'
import ParisPic from '../assets/img/Paris.jpg'
import LondonPic from '../assets/img/London.jpg'

class Promotions extends Component {
  render () {
    return (
      <>
        <Media>
          <img
            width={200}
            height={130}
            className='mr-3'
            src={NhaTrangPic}
            alt='Nha Trang'
          />
          <Media.Body>
            <h5>Ho Chi Minh - Nha Trang (30% OFF)</h5>
            <p>
              <b>Promotion Code: 7EH8K4</b>
            </p>
            <p>
              Nha Trang City is located in a valley surrounded by the mountains
              in the north, west and south and border with East Sea in the east.
              Cai River ...
            </p>
          </Media.Body>
        </Media>
        <Media>
          <img
            width={200}
            height={130}
            className='mr-3'
            src={TokyoPic}
            alt='Tokyo'
          />
          <Media.Body>
            <h5>Hanoi - Japan (Tokyo) FLASH SALE 10% OFF</h5>
            <p>
              <b>Promotion Code: KMN32K</b>
            </p>
            <p>
              Nha Trang City is located in a valley surrounded by the mountains
              in the north, west and south and border with East Sea in the east.
              Cai River ...
            </p>
          </Media.Body>
        </Media>
        <Media>
          <img
            width={200}
            height={130}
            className='mr-3'
            src={ParisPic}
            alt='Paris'
          />
          <Media.Body>
            <h5>Ho Chi Minh - France (Paris) (15% OFF)</h5>
            <p>
              <b>Promotion Code: 90J2KD</b>
            </p>
            <p>
              Nha Trang City is located in a valley surrounded by the mountains
              in the north, west and south and border with East Sea in the east.
              Cai River ...
            </p>
          </Media.Body>
        </Media>
        <Media>
          <img
            width={200}
            height={130}
            className='mr-3'
            src={LondonPic}
            alt='London'
          />
          <Media.Body>
            <h5>Ho Chi Minh - London (UK) (5% OFF)</h5>
            <p>
              <b>Promotion Code: M23NDA</b>
            </p>
            <p>
              Nha Trang City is located in a valley surrounded by the mountains
              in the north, west and south and border with East Sea in the east.
              Cai River ...
            </p>
          </Media.Body>
        </Media>
      </>
    )
  }
}

export default Promotions
