import React from 'react'
import { Table, Button } from 'react-bootstrap'
import '../assets/css/booking-confirm-component.css'
import { connect } from 'react-redux'
import { deleteBooking, getAllBooking } from '../redux/actions/actions'
import { withRouter } from 'react-router-dom'

const mapStateToProps = (state) => ({
  reduxState: state
})

const mapDispatchToProps = dispatch => {
  return {
    deleteBooking: (id) => dispatch(deleteBooking(id)),
    getAllBooking: () => dispatch(getAllBooking())
  }
}

class MyBook extends React.Component {
  componentDidMount = (props) => {
    this.props.getAllBooking()
  }
  
  handleOnDeleteButtonClicked (id) {
    this.props.deleteBooking(id)
  }

  render () {
    var bookList = this.props.reduxState.booked.map(book => {
      return (
        <>
          <tr key={book.id}>
            <td>{book.id}</td>
            <td>{book.type === 0 ? 'Return' : 'No Return'}</td>
            <td>{book.departure.airport} ({book.departure.time.toString()}) <b>{book.departure.class}</b></td>
            <td>{book.arrival.airport} ({book.arrival.time.toString()}) <b>{book.arrival.class}</b></td>
            <td><b>Adult:</b> {book.travellers.adult} <b>Children:</b> {book.travellers.children} <b>Infant:</b> {book.travellers.infant}</td>
            <td>{book.promotion_code}</td>
            <td><Button type='button' variant='danger' onClick={this.handleOnDeleteButtonClicked.bind(this, { id: book.id })}><b>Delete</b></Button></td>
          </tr>
        </>
      )
    })

    let bookGrid = 'You don\'t have any book yet.'

    if (this.props.reduxState.booked.length !== 0) {
      bookGrid = (
        <Table striped bordered hover>
          <thead style={{ backgroundColor: '#c82333', color: 'white' }}>
            <tr>
              <td>ID</td>
              <td>Type</td>
              <td>Departure Info</td>
              <td>Return Info</td>
              <td>Ticket</td>
              <td>Promotion</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>
            {bookList}
          </tbody>
        </Table>
      )
    }

    return (
      <>
        {bookGrid}
      </>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyBook))
