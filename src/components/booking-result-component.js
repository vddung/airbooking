import React from 'react'
import { Link } from 'react-router-dom'

const BookingResult = (props) => {
  return (
    <>
      <h2>Booked successfully!</h2>
      <Link to='/' className='btn btn-outline-secondary btn-sm' style={{ marginRight: '5px' }}>Back to Home</Link>
      <Link to={props.nextStep} className='btn btn-danger btn-sm'>Continue booking</Link>
    </>
  )
}

export default BookingResult
