import React, { Component } from 'react'
import { Jumbotron } from 'react-bootstrap'
import { Link } from 'react-router-dom'

class TopMenu extends Component {
  render () {
    return (
      <Jumbotron className='top-menu'>
        <h1>
          <Link to='/'>Air Booking Portal</Link>
        </h1>
      </Jumbotron>
    )
  }
}

export default TopMenu
