import React, * as react from 'react'
import { Row, Col, Card, Image, Badge } from 'react-bootstrap'
import * as Moment from 'moment'

import 'react-datepicker/dist/react-datepicker.css'
import '../assets/css/form.css'

import OutboundFlightIco from '../assets/img/outbound-flight.png'
import TailIIco from '../assets/img/icn_tail_EK.svg'
import ConnFlightIco from '../assets/img/connection-flight.png'

class FlightScheduleCard extends react.Component {
  getTime (date, formatString) {
    return Moment(date).format(formatString)
  }

  hightlightSelectWeekday (weekday) {
    return this.props.flight.departure.weekday === weekday ? 'dark' : 'light'
  }

  render () {
    const connFlights = this.props.flight.connections.map(conn => {
      var connectionTime = ''
      if (conn.connection_required === true) {
        connectionTime = <Card.Text><b>Connection</b> in {conn.arrival.airport} ({conn.arrival.airport_id}}) : {conn.connectionTime}}</Card.Text>
      }

      return (
        <>
          <hr />
          <Card.Body className='connection-flight-card-body' as={Row}>
            <Col className='departure-widget' md={1}>
              <Card.Text>{conn.departure.airport_id}</Card.Text>
              <Card.Text><b>{this.getTime(conn.departure.time, 'HH:mm')}</b></Card.Text>
            </Col>
            <Col className='connection-flight-time-id-widget' md={1}>
              <Image className='connection-flight-img' src={ConnFlightIco} width={25} height={22} />
            </Col>
            <Col className='arrival-widget' md={1}>
              <Card.Text>{conn.arrival.airport_id}</Card.Text>
              <Card.Text><b>{this.getTime(conn.arrival.time, 'HH:mm')}</b></Card.Text>
            </Col>
            <Col className='connection-flight-time-id-widget' md={2}>
              <Card.Text><b>{conn.onflight}</b></Card.Text>
            </Col>
            <Col className='departure-widget' md={1}>
              <Image className='flight-tail-img-center' variant='top' src={TailIIco} width={28} height={38} />
              <Card.Text>{conn.plane}</Card.Text>
            </Col>
            <Col className='arrival-widget' md={2}>
              <Card.Text><b>{conn.plane_type}}</b></Card.Text>
              <Card.Link href='#'>View Services</Card.Link>
            </Col>
            <Col className='arrival-widget' md={4}>
              {connectionTime}
            </Col>
          </Card.Body>
        </>
      )
    })

    return (
      <Card className='flight-status-card'>
        <Card.Body style={{ height: '60px' }}>
          <Image className='float-right' src={OutboundFlightIco} width={60} height={40} rounded />
          <Card.Title><h4>{this.props.flight.direction} from {this.props.flight.departure.airport} ({this.props.flight.departure.airport_id}) to {this.props.flight.arrival.airport} ({this.props.flight.arrival.airport_id})</h4></Card.Title>
        </Card.Body>
        <hr />
        <Card.Body as={Row}>
          <Col className='departure-widget' md={3}>
            <Card.Text>Departure</Card.Text>
            <Card.Text><h1><b>{this.getTime(this.props.flight.departure.time, 'HH:mm')}</b></h1></Card.Text>
            <Card.Text>{this.props.flight.departure.airport} ({this.props.flight.departure.airport_id})</Card.Text>
          </Col>
          <Col className='flight-time-id-widget' md={2}>
            <span>
              <Image className='flight-tail-img' variant='top' src={TailIIco} width={37} height={45} />
              <Image className='flight-tail-img-co float-left' variant='top' src={TailIIco} width={37} height={45} />
            </span>
            <Card.Text>{this.props.flight.planes.join(' / ')}</Card.Text>
            <Card.Text><hr /></Card.Text>
            <Card.Text>{this.props.flight.onflight}</Card.Text>
            <Card.Text>{this.props.flight.stopover}</Card.Text>
          </Col>

          <Col className='arrival-widget' md={3}>
            <Card.Text>Arrival</Card.Text>
            <Card.Text><h1><b>{this.getTime(this.props.flight.arrival.time, 'HH:mm')}</b></h1></Card.Text>
            <Card.Text>{this.props.flight.arrival.airport} ({this.props.flight.arrival.airport_id})</Card.Text>
          </Col>
          <Col className='weekdays-widget' md={4}>
            <Card.Text>Days</Card.Text>
            <Card.Text>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('1')}>Mo</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('2')}>Tu</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('3')}>We</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('4')}>Th</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('5')}>Fr</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('6')}>Sa</Badge>
              <Badge className='weekday-badge' variant={this.hightlightSelectWeekday('7')}>Su</Badge>
            </Card.Text>
            <Card.Text>Stops</Card.Text>
            <Card.Text><b>{this.props.flight.stops}</b></Card.Text>
          </Col>
        </Card.Body>
        {connFlights}
      </Card>
    )
  }
}

export default FlightScheduleCard
