import React, { Component } from 'react'
import { ListGroup, ListGroupItem } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import '../assets/css/form.css'

class LeftMenu extends Component {
  render () {
    return (
      <ListGroup>
        <ListGroupItem action>
          <Link to='/myBook' className='router-link'>
            My Book
          </Link>
        </ListGroupItem>
        <ListGroupItem action>
          <Link to='/bookingWizard' className='router-link'>
            Booking
          </Link>
        </ListGroupItem>
        <ListGroupItem action>
          <Link to='/schedule'>Flights Status</Link>
        </ListGroupItem>
        <ListGroupItem action>
          <Link to='/promotions'>Promotions</Link>
        </ListGroupItem>
      </ListGroup>
    )
  }
}

export default LeftMenu
