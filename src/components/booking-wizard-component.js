import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Booking from './booking-component'
import BookingConfirmation from './booking-confirm-component'
import BookingResult from './booking-result-component'

class BookingWizard extends Component {
  render () {
    return (
      <>
        <Switch>
          <Route path={`${this.props.match.path}`} exact render={() => <Booking nextStep={`${this.props.match.path}/confirm`} />} />
          <Route path={`${this.props.match.path}/confirm`} render={() => <BookingConfirmation prevStep={`${this.props.match.path}`} nextStep={`${this.props.match.path}/result`} />} />
          <Route path={`${this.props.match.path}/result`} render={() => <BookingResult nextStep={`${this.props.match.path}`} />} />
        </Switch>
      </>
    )
  }
}

export default BookingWizard
