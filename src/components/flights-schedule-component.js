import React, { Component } from 'react'
import FlightScheduleCard from './flight-schedule-card-component'
import { Row, Col, Form, Button } from 'react-bootstrap'
import { FLIGHTS } from '../data/FLIGHTS'

import 'react-datepicker/dist/react-datepicker.css'
import '../assets/css/form.css'

class FlightsSchedule extends Component {
  constructor (props) {
    super(props)

    this.state = {
      flight_id: '',
      searchResult: []
    }

    this.checkStatus = this.checkStatus.bind(this)
    this.updateState = this.updateState.bind(this)
  }

  handleChange (date) {
    this.setState({
      startDate: date
    })
  }

  updateState (event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  checkStatus () {
    var searchResult = []

    FLIGHTS.forEach(element => {
      if (element.id === this.state.flight_id) {
        searchResult.push(element)
      }
    })

    this.setState({
      searchResult: searchResult
    })
  }

  render () {
    const flightCards = this.state.searchResult.map(flight => (
      <FlightScheduleCard key={flight} flight={flight} />
    ))

    return (
      <>
        <Row>
          <Col md>
            <Form>
              <Form.Row>
                <Form.Group as={Col} controlId='frmSearchFlight'>
                  <Form.Control
                    name='flight_id'
                    type='input'
                    placeholder='Flight ID. Ex: 81124392'
                    value={this.state.flight_id}
                    onChange={this.updateState}
                  />
                </Form.Group>
                <Form.Group controlId='frmReturnDate'>
                  <Button
                    type='button'
                    variant='danger'
                    className='float-right'
                    onClick={this.checkStatus}
                  >
                    <b>Check Status</b>
                  </Button>
                </Form.Group>
              </Form.Row>
              <hr />
              {flightCards}
            </Form>
          </Col>
        </Row>
      </>
    )
  }
}

export default FlightsSchedule
