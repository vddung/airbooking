import React, { Component } from 'react'
import { Jumbotron, ListGroup, ListGroupItem, Button } from 'react-bootstrap'

class Index extends Component {
  render () {
    return (
      <Jumbotron>
        <h1>Air Booking</h1>
        <p>This is an online air booking website.</p>
        <p>
          <ListGroup>
            <ListGroupItem variant='danger'>
              <h5>
                <b>Site Map</b>
              </h5>
            </ListGroupItem>
            <ListGroupItem>
              <b>Booking</b>: Book a new flight.
            </ListGroupItem>
            <ListGroupItem>
              <b>Flight Status</b>: Search for flight status with flight number.
            </ListGroupItem>
            <ListGroupItem>
              <b>Promotion</b>: Find our running offers here.
            </ListGroupItem>
          </ListGroup>
        </p>
        <p>
          <Button variant='danger'>Start Booking</Button>
        </p>
      </Jumbotron>
    )
  }
}

export default Index
