import React from 'react'
import { Table, Button } from 'react-bootstrap'
import '../assets/css/booking-confirm-component.css'
import { connect } from 'react-redux'
import { addBooking, resetCurrentBooking } from '../redux/actions/actions'
import { Link, withRouter } from 'react-router-dom'

const mapStateToProps = (state) => ({
  reduxState: state
})

const mapDispatchToProps = dispatch => {
  return {
    addBooking: (booking) => dispatch(addBooking(booking)),
    resetCurrentBooking: () => dispatch(resetCurrentBooking())
  }
}

class BookingConfirmation extends React.Component {
  handleOnSubmitButtonClicked () {
    this.props.addBooking(Object.assign({}, this.props.reduxState.currentBooking))
    this.props.resetCurrentBooking()
    this.props.history.push(this.props.nextStep)
  }

  render () {
    return (
      <>
        <Table>
          <tbody>
            <tr>
              <td className='rowTitle'>ID</td>
              <td className='rowContent'>{this.props.reduxState.currentBooking.id}</td>
            </tr>
            <tr>
              <td className='rowTitle'>Return?</td>
              <td className='rowContent'>{this.props.reduxState.currentBooking.type === 0 ? 'Yes' : 'No'}</td>
            </tr>
            <tr>
              <td className='rowTitle'>Departure Info</td>
              <td className='rowContent'>{this.props.reduxState.currentBooking.departure.airport} ({this.props.reduxState.currentBooking.departure.time.toString()}) <b>{this.props.reduxState.currentBooking.departure.class}</b></td>
            </tr>
            <tr>
              <td className='rowTitle'>Return Info</td>
              <td className='rowContent'>{this.props.reduxState.currentBooking.arrival.airport} ({this.props.reduxState.currentBooking.arrival.time.toString()}) <b>{this.props.reduxState.currentBooking.arrival.class}</b></td>
            </tr>
            <tr>
              <td className='rowTitle'>Ticket</td>
              <td className='rowContent'><b>Adult:</b> {this.props.reduxState.currentBooking.travellers.adult} <b>Children:</b> {this.props.reduxState.currentBooking.travellers.children} <b>Infant:</b> {this.props.reduxState.currentBooking.travellers.infant}</td>
            </tr>
            <tr>
              <td className='rowTitle'>Promotion Code</td>
              <td className='rowContent'>{this.props.reduxState.currentBooking.promotion_code}</td>
            </tr>
          </tbody>
        </Table>
        <Link to={this.props.prevStep}>Back</Link>
        <Button type='button' variant='danger' className='float-right' onClick={this.handleOnSubmitButtonClicked.bind(this)}><b>Submit</b></Button>
      </>
    )
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BookingConfirmation))
