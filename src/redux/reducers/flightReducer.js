import { addFlight } from '../actions/actions'

export function flightReducer (state = [], action) {
  switch (action.type) {
    case 'ADD_FLIGHT':
      return [...state, action.flight]
    case 'DELETE_FLIGHT':
      return state.filter((value, index, arr) => {
        return value.id === action.flightId
      })
    default:
      return state
  }
}
