var initialState = {
  id: '',
  type: 0,
  departure: {
    airport: '',
    time: new Date(),
    class: 'Economic Class'
  },
  arrival: {
    airport: '',
    time: new Date(),
    class: 'Economic Class'
  },
  travellers: {
    adult: 0,
    children: 0,
    infant: 0
  },
  promotion_code: ''
}

export function currentBookingReducer (state = initialState, action) {
  switch (action.type) {
    case 'UPDATE_CURRENT_BOOKING':
      return Object.assign({}, state, action.booking)
    case 'RESET_CURRENT_BOOKING':
      return Object.assign({}, initialState)
    default:
      return state
  }
}

export function bookingReducer (state = [], action) {
  switch (action.type) {
    case 'ADD_BOOKING':
      var find = state.filter(value => {
        return value.id === action.booking.id
      })

      if (find.length === 0) {
        return [...state, action.booking]
      } else {
        return state
      }
    case 'DELETE_BOOKING':
      var stateWithFilter = state.filter(value => {
        return value.id !== action.id.id
      })
      console.log(stateWithFilter)
      return [...stateWithFilter]
    default:
      return state
  }
}
