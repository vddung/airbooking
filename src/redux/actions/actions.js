import axios from 'axios'

export function addFlight (flight) {
  return { type: 'ADD_FLIGHT', flight }
}

export function updateCurrentBooking (booking) {
  return { type: 'UPDATE_CURRENT_BOOKING', booking }
}

export function resetCurrentBooking (booking) {
  return { type: 'RESET_CURRENT_BOOKING' }
}

const client = axios.create({
  baseURl: 'http://localhost:3000/'
})

axios.defaults.baseURL = 'http://localhost:3000/'

export function addBooking (booking) {
  console.log(booking)
  return (dispatch, getState) => {
    axios
      .post('/booking', booking)
      .then(res => {
        console.log(res)
        dispatch({ type: 'ADD_BOOKING', booking })
      })
      .catch(err => {
        console.log(err)
      })
  }
}

export function getAllBooking () {
  return (dispatch, getState) => {
    axios
      .get('/booking')
      .then(res => {
        console.log(res)
        res.data.forEach(booking => {
          dispatch({ type: 'ADD_BOOKING', booking })
        })
      })
      .catch(err => {
        console.log(err)
      })
  }
}

export function deleteBooking (id) {
  console.log(id)
  return (dispatch, getState) => {
    axios
      .delete('/booking', { data: id })
      .then(res => {
        console.log(res)
        dispatch({ type: 'DELETE_BOOKING', id })
      })
      .catch(err => {
        console.log(err)
      })
  }
}
