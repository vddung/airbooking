import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { composeWithDevTools } from 'remote-redux-devtools'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { flightReducer } from '../reducers/flightReducer'
import {
  bookingReducer,
  currentBookingReducer
} from '../reducers/bookingReducer'

var combinedReducers = combineReducers({
  flights: flightReducer,
  currentBooking: currentBookingReducer,
  booked: bookingReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export const store = createStore(
  combinedReducers,
  composeEnhancers(applyMiddleware(logger, thunk))
)
