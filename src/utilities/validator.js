export function isMaxLength (value) {
  if (value.length >= 10) {
    return true
  } else return false
}

export function isEmpty (value) {
  if (value.trim().length === 0) {
    return true
  } else return false
}
