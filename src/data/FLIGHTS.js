export const FLIGHTS = [
  {
    id: '81124392',
    direction: 'Outbound',
    planes: ['EK022', 'EK316'],
    onflight: '23h 15m',
    stopover: '7h 5m',
    stops: 1,
    departure: {
      time: new Date(2019, 8, 10, 10),
      weekday: '5',
      airport: 'Manchester',
      airport_id: 'MAN'
    },
    arrival: {
      time: new Date(2019, 8, 10, 17, 15),
      airport: 'OSAKA',
      airport_id: 'KIX'
    },
    connections: [
      {
        onflight: '6h 55m',
        plane: 'EK022',
        plane_type: 'Airbus 380',
        connection_required: true,
        connection_time: '7h 5m',
        departure: {
          time: new Date(2019, 8, 10, 10),
          airport: 'Manchester',
          airport_id: 'MAN'
        },
        arrival: {
          time: new Date(2019, 8, 10, 19, 55),
          airport: 'Dubai',
          airport_id: 'DXB'
        }

      },
      {
        onflight: '9h 15m',
        plane: 'EK316',
        plane_type: 'Airbus 380',
        connection_required: false,
        connection_time: '',
        departure: {
          time: new Date(2019, 8, 10, 3, 0),
          airport: 'Manchester',
          airport_id: 'MAN'
        },
        arrival: {
          time: new Date(2019, 8, 10, 19, 15),
          airport: 'Dubai',
          airport_id: 'DXB'
        }

      }
    ]
  },
  {
    id: '81124393',
    direction: 'Inbound',
    planes: ['EK317', 'EK017'],
    onflight: '20h 20m',
    stopover: '2h 35m',
    stops: 1,
    departure: {
      time: new Date(2019, 8, 10, 23, 45),
      weekday: '5',
      airport: 'Osaka',
      airport_id: 'KIX'
    },
    arrival: {
      time: new Date(2019, 8, 10, 12, 5),
      airport: 'Manchester',
      airport_id: 'MAN'
    },
    connections: [
      {
        onflight: '10h 5m',
        plane: 'EK022',
        plane_type: 'Airbus 380',
        connection_required: true,
        connection_time: '2h 35m',
        departure: {
          time: new Date(2019, 8, 10, 23, 45),
          airport: 'Osaka',
          airport_id: 'KIX'
        },
        arrival: {
          time: new Date(2019, 8, 10, 4, 50),
          airport: 'Manchester',
          airport_id: 'MAN'
        }

      },
      {
        onflight: '7h 40m',
        plane: 'EK017',
        plane_type: 'Airbus 380',
        connection_required: false,
        connection_time: '',
        departure: {
          time: new Date(2019, 8, 10, 7, 25),
          airport: 'Osaka',
          airport_id: 'KIX'
        },
        arrival: {
          time: new Date(2019, 8, 10, 12, 5),
          airport: 'Manchester',
          airport_id: 'MAN'
        }

      }
    ]
  }
]
